<?php

if(!defined('BASEPATH')) exit('No direct script access allowed.');

class Home extends Main_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$data['title'] = "SiOren | Homepage";
		$data['content'] = "home/vhome";
		$this->template->main_template($data);
	}

	function about(){
		$data['title'] = "SiOren | About Us";
		$data['content'] = "home/vabout";
		$this->template->main_template($data);
	}

}

?>