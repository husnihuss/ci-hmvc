<?php

if(!defined('BASEPATH')) exit('No direct script access allowed.');

class Login extends Main_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$data['title'] = "SiOren | Login";
		$this->load->view('vlogin', $data);
	}

	public function logout(){
		$data['title'] = "SiOren | Login";
		$this->load->view('vlogin', $data);
	}

}

?>