<?php

if(!defined('BASEPATH')) exit('No direct script access allowed.');

class Admin extends Main_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$data['title'] = "SiOren | Admin Dashboard";
		$data['content'] = "admin/dashboard";
		$this->template->admin_template($data);
	}

	function user(){
		$data['title'] = "SiOren | User Profile";
		$data['content'] = "admin/user";
		$this->template->admin_template($data);
	}

}

?>