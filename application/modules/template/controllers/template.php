<?php

if(!defined('BASEPATH')) exit('No direct script access allowed.');

class Template extends Main_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		echo "Hello, Good Boy. This HMVC. On Template Module.";
	}

	function main_template($data = null){
		$this->load->view('template/vmain_template', $data);
	}

	function admin_template($data = null){
		$this->load->view('template/vadmin_template', $data);
	}

}

?>