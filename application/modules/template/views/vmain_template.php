<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title ?></title>
	<script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/jquery-1.10.2.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
	<script src="<?php echo base_url('assets/js/login-register.js') ?>"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css') ?>">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>"> 
	<link rel="stylesheet" href="<?php echo base_url('assets/css/login-register.css'); ?>">
	<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css">
</head>
<body>

<!-- NAVBAR -->
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Brand</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a data-toggle="modal" href="javascript:void(0)" onclick="openLoginModal();">Login</a></li>
        <li><a data-toggle="modal" href="javascript:void(0)" onclick="openRegisterModal();">Register</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!-- END NAVBAR -->

<!-- CONTENT -->
<div class="container-fluid wrapper">
	<div class="row">
		<div class="col-md-12">
			<?php $this->load->view($content) ?>
			<br>
		</div>
	</div>
</div>
<!-- END CONTENT -->

<!-- MODAL -->
<div class="modal fade login" id="loginModal">
  <div class="modal-dialog login animated">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Login with</h4>
        </div>
        <div class="modal-body">  
            <div class="box">
                 <div class="content">
                    <div class="social">
                        <a class="circle github" href="/auth/github">
                            <i class="fa fa-github fa-fw"></i>
                        </a>
                        <a id="google_login" class="circle google" href="/auth/google_oauth2">
                            <i class="fa fa-google-plus fa-fw"></i>
                        </a>
                        <a id="facebook_login" class="circle facebook" href="/auth/facebook">
                            <i class="fa fa-facebook fa-fw"></i>
                        </a>
                    </div>
                    <div class="division">
                        <div class="line l"></div>
                          <span>or</span>
                        <div class="line r"></div>
                    </div>
                    <div class="error"></div>
                    <div class="form loginBox">
                        <form method="post" action="/login" accept-charset="UTF-8">
                        <input id="email" class="form-control" type="text" placeholder="Email" name="email">
                        <input id="password" class="form-control" type="password" placeholder="Password" name="password">
                        <input class="btn btn-default btn-login" type="button" value="Login" onclick="loginAjax()">
                        </form>
                    </div>
                 </div>
            </div>
            <div class="box">
                <div class="content registerBox" style="display:none;">
                 <div class="form">
                    <form method="post" html="{:multipart=>true}" data-remote="true" action="/register" accept-charset="UTF-8">
                    <input id="email" class="form-control" type="text" placeholder="Email" name="email">
                    <input id="password" class="form-control" type="password" placeholder="Password" name="password">
                    <input id="password_confirmation" class="form-control" type="password" placeholder="Repeat Password" name="password_confirmation">
                    <input class="btn btn-default btn-register" type="submit" value="Create account" name="commit">
                    </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="forgot login-footer">
                <span>Looking to 
                     <a href="javascript: showRegisterForm();">create an account</a>
                ?</span>
            </div>
            <div class="forgot register-footer" style="display:none">
                 <span>Already have an account?</span>
                 <a href="javascript: showLoginForm();">Login</a>
            </div>
        </div>        
      </div>
  </div>
</div>
<!-- END MODAL -->

</body>
</html>